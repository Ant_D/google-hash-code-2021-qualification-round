## Google Hash Code Qualification Round 2021

[See problem](https://storage.googleapis.com/coding-competitions.appspot.com/HC/2021/hashcode_2021_online_qualification_round.pdf)

[Download tests](https://storage.googleapis.com/coding-competitions.appspot.com/HC/2021/qualification_round_2021.in.zip)